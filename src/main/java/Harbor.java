import by.sc.harbor.entity.Operation;
import by.sc.harbor.thread.Port;
import by.sc.harbor.thread.Ship;
import by.sc.harbor.util.Configurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Harbor {

    private static final Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {
        LOG.info("Старт главного потока.");

        /* создаем объект порт */
        Port port = Port.createPort();

        /* создаем очередь потоков кораблей */
        int shipCount = Integer.parseInt(Configurator.getProperty("ship.count"));
        if (shipCount == 0) {
            shipCount = 5;
        }
        int pierCount = Integer.parseInt(Configurator.getProperty("pier.count"));
        if (pierCount == 0) {
            shipCount = 3;
        }
        ExecutorService harborDispatcher = Executors.newFixedThreadPool(pierCount);
        for (int i = 1; i < shipCount + 1; i++) {
            Operation oper = Operation.LOAD;
            if (i % 2 == 0) {
                oper = Operation.UNLOAD;
            }
            harborDispatcher.submit(new Thread(new Ship("Корабль #" + i, new Random().nextInt(10), 10, port, oper)));
        }
        harborDispatcher.shutdown();

        LOG.info("Конец работы главного потока.");
    }

}
