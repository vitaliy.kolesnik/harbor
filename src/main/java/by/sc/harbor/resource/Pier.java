package by.sc.harbor.resource;

import by.sc.harbor.entity.Container;
import by.sc.harbor.util.Configurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Pier implements Iterable<Container> {
    private static final Logger LOG = LogManager.getLogger();

    private Queue<Container> containers = null;
    private int pierNum;

    public Pier(int number) {
        pierNum = number;
        int pierCapacity = Integer.parseInt(Configurator.getProperty("pier.capacity"));
        containers = new PriorityQueue<>(pierCapacity);
        List<Container> itemList = generateContainers(pierCapacity);
        containers.addAll(itemList);
        LOG.info("Открыт причал #" + number + ". В хранилище " + containers.size() + " контейнеров.");
    }

    public Container getContainer() {
        if (!containers.isEmpty()) {
            return containers.poll();
        } else
            return null;
    }

    public boolean setContainer(Container container) {
        return containers.offer(container);
    }

    public int getPierNum() {
        return pierNum;
    }

    @Override
    public Iterator<Container> iterator() {
        return containers.iterator();
    }

    private static List<Container> generateContainers(int pierCapacity) {
        Random generator = new Random();
        int length = generator.nextInt(pierCapacity);
        Container[] containers = new Container[length];
        for (int i = 0; i < length; i++) {
            containers[i] = new Container(generator.nextInt(500));
        }
        return Arrays.asList(containers);
    }
}
