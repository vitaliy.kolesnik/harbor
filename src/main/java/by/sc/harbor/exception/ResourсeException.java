package by.sc.harbor.exception;

public class ResourсeException extends Exception {

    public ResourсeException() {
        super();
    }

    public ResourсeException(String message) {
        super(message);
    }

    public ResourсeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourсeException(Throwable cause) {
        super(cause);
    }
}
