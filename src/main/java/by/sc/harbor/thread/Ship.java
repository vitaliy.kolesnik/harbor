package by.sc.harbor.thread;

import by.sc.harbor.entity.Container;
import by.sc.harbor.entity.Operation;
import by.sc.harbor.exception.ResourсeException;
import by.sc.harbor.resource.Pier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Ship implements Runnable {
    static final Logger LOG = LogManager.getLogger();

    private String name;                    /* наименование */
    private int currentCargo;               /* текущий вес груза */
    private int maxCargo;                   /* максимальный все груза */
    private ArrayDeque<Container> hold;     /* хранилище контейнеров */
    private Operation oper;

    private Port port;
    private Pier pier;

    private Lock lock = new ReentrantLock();

    public Ship(String name, int currentCargo, int maxCargo, Port port, Operation oper) {
        this.name = name;
        this.currentCargo = currentCargo;
        this.maxCargo = maxCargo;
        this.hold = new ArrayDeque<>(maxCargo);
        this.port = port;
        this.oper = oper;
        /* заполняем хранилище */
        fillShipCargo();
    }

    private void fillShipCargo() {
        if (maxCargo > 0) {
            Container[] containers = new Container[maxCargo];
            for (int i = 1; i < currentCargo + 1; i++) {
                containers[i] = new Container(i + 1000);
                hold.add(containers[i]);
            }
        }
    }

    @Override
    public void run() {
        moorToPier(); // пришвартоваться
        lock.lock();
        if (oper == Operation.LOAD) {
            loadCargo();
        } else {
            unloadCargo();
        }
        breakOffPier(); // отшвартоваться
        lock.unlock();
    }

    private void breakOffPier() {
        try {
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(500));
            LOG.info(this + " отшвартовался от причала " + pier.getPierNum());
        } catch (InterruptedException e) {
            LOG.error("Ошибка внезапного прерывания потока.", e);
        } finally {
            port.returnPier(pier);
            LOG.info("Причал #" + pier.getPierNum() + " свободен.");
        }
    }

    private void unloadCargo() {
        if (currentCargo == 0) {
            LOG.info("Корабль пустой! Выполняется погрузка.");
            loadCargo();
            return;
        }
        for (int i = 1; i < maxCargo + 1; i++) {
            Container item = hold.poll();
            if (pier.setContainer(item)) {
                LOG.info(this + " выгрузил контейнер #" + item.getId());
                currentCargo--;
            } else {
                LOG.error("Склад на пирсе " + pier.getPierNum() + " переполнен.");
                return;
            }
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {
                LOG.error("Ошибка внезапного прерывания потока.", e);
            }
        }
        currentCargo = 0;
    }

    private void loadCargo() {
        int amountToLoad = maxCargo - currentCargo;
        for (int i = 1; i < amountToLoad + 1; i++) {
            Container item = pier.getContainer();
            currentCargo++;
            if (item == null) { // если в порту нет товаров, загрузка
                LOG.info("В порту закончился груз.");
                return;
            }
            if (hold.offer(item)) {
                LOG.info(this + " загрузил контейнер #" + item.getId());
            } else {
                LOG.info("Не хватает места на складе корабля!");
                return;
            }
            try {
                TimeUnit.MILLISECONDS.sleep(50);
            } catch (InterruptedException e) {
                LOG.error("Ошибка внезапного прерывания потока.", e);
            }
        }
    }

    private void moorToPier() {
        try {
            getPier();
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
            String operation = (oper == Operation.LOAD) ? "для погрузки." : "для разгрузки.";
            LOG.info(this + " пришвартовался на " + pier.getPierNum() + " причал " + operation);
        } catch (InterruptedException e) {
            LOG.error("Ошибка внезапного прерывания потока.", e);
        }
    }

    private void getPier() throws InterruptedException {
        do {
            try {
                pier = port.getPier();
            } catch (ResourсeException e) {
                LOG.error(e.getMessage());
            }
            if (pier == null) {
                LOG.info(this + " ждет своей очереди.");
                TimeUnit.MILLISECONDS.sleep(500);
            }
        } while (pier == null);
    }

    @Override
    public String toString() {
        return name + " [" + currentCargo + "/" + maxCargo + "]";
    }
}
