package by.sc.harbor.thread;

import by.sc.harbor.exception.ResourсeException;
import by.sc.harbor.resource.Pier;
import by.sc.harbor.util.Configurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Port {
    private static final Logger LOG = LogManager.getLogger();

    private static Semaphore semaphore;
    private ArrayDeque<Pier> piers = new ArrayDeque<>();
    private static final Port PORT = new Port();
    private Lock lock = new ReentrantLock();

    private Port() {
        LOG.info("Порт открыт.");
    }

    public static Port createPort() {
        int pierCount = Integer.parseInt(Configurator.getProperty("pier.count"));
        PORT.piers.addAll(generatePiers(pierCount));
        semaphore = new Semaphore(pierCount, true);
        return PORT;
    }

    public void returnPier(Pier pier) {
        piers.add(pier); // возвращение пирса в пул
        semaphore.release();
    }

    public Pier getPier() throws ResourсeException {
        lock.lock();
        while (true) {
            try {
                if (semaphore.tryAcquire(100, TimeUnit.MILLISECONDS)) {
                    Pier pier = piers.poll();
                    if (pier != null) {
                        LOG.info("Причал #" + pier.getPierNum() + " зарезервирован.");
                    }
                    lock.unlock();
                    return pier;
                }
            } catch (InterruptedException e) {
                throw new ResourсeException("Истекло время ожидания.");
            }
        }
    }

    private static Collection<Pier> generatePiers(int pierCount) {
        ArrayDeque<Pier> list = new ArrayDeque<>();
        for (int i = 1; i <= pierCount; i++) {
            list.add(new Pier(i));
        }
        return list;
    }
}
