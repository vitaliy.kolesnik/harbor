package by.sc.harbor.entity;

public class Container implements Comparable<Container> {
    private int id;

    public Container(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Container container = (Container) o;

        return id == container.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Container o) {
        return o.getId() - this.id;
    }
}
