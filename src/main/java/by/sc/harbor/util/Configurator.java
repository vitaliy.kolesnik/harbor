package by.sc.harbor.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configurator {
    static final Logger LOG = LogManager.getLogger();

    public static String getProperty(String key) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("/resources/config.properties"));
            return properties.getProperty(key);
        } catch (IOException e) {
            LOG.error("Ошибка: отсутствует файл параметров приложения.\n" + e.getMessage());
        }
        return "";
    }

}
